from urllib.request import urlopen
from urllib.error import URLError
import json
from datetime import datetime
import os
import csv
import numpy as np
from tabulate import tabulate


def parse_utm(url, ip="localhost", name="noname"):
    try:
        response = urlopen(url, timeout=5)
        data_json = json.loads(response.read())
        # "%Y-%m-%d %H:%M:%S %z"
        rsa_startDate = datetime.strptime(data_json["rsa"]["startDate"], "%Y-%m-%d %H:%M:%S %z")
        rsa_expireDate = datetime.strptime(data_json["rsa"]["expireDate"], "%Y-%m-%d %H:%M:%S %z")
        rsa_expire_days = rsa_expireDate.replace(tzinfo=None) - datetime.today()
        rsa_isValid = data_json["rsa"]["isValid"]

        gost_startDate = datetime.strptime(data_json["gost"]["startDate"], "%Y-%m-%d %H:%M:%S %z")
        gost_expireDate = datetime.strptime(data_json["gost"]["expireDate"], "%Y-%m-%d %H:%M:%S %z")
        gost_expire_days = gost_expireDate.replace(tzinfo=None) - datetime.today()
        gost_isValid = data_json["gost"]["isValid"]

        parse_result = [[str(name), str(ip), rsa_expireDate.strftime("%Y-%m-%d"), rsa_expire_days.days, rsa_isValid,
                         gost_expireDate.strftime("%Y-%m-%d"), gost_expire_days.days, gost_isValid]]

    except URLError as e:
        if hasattr(e, 'reason'):
            print('We failed to reach a server.')
            print('Reason: ', e.reason)
        elif hasattr(e, 'code'):
            print('The server couldn\'t fulfill the request.')
            print('Error code: ', e.code)

        parse_result = [[str(name), str(ip), "timeout", "timeout", "timeout", "timeout", "timeout", "timeout"]]

    print(parse_result)
    return parse_result


def check_utm():
    result = [[]]
    table_headers = ['Name', 'IP', 'RSA expire date', 'Days to expire', 'RSA Valid', 'GOST expire date', 'Days to expire',
                     'RSA Valid']

    if os.path.isfile('ip_list.txt'):
        with open('ip_list.txt', encoding = 'utf-8') as f:
            ip_list = list(f)

        for ip in ip_list:
            ip = ip.strip().split('\t')
            url = f"http://{ip[0]}:8080/api/info/list"
            result += parse_utm(url, ip[0], ip[1])

    else:
        url = f"http://localhost:8080/api/info/list"
        parse_utm(url)

    with open('parse_result.csv', 'w', newline='') as f:
        write = csv.writer(f)
        write.writerow(table_headers)
        write.writerows(result)

    with open('parse_result.txt', 'w') as f:
        f.write(tabulate(result, headers=table_headers))


# ------------------
check_utm()
