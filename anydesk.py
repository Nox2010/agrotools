import os
import shutil
import psutil
import subprocess
import time

anydeskExe = r'C:\Program Files (x86)\AnyDesk\AnyDesk.exe'
rmngPath = os.getenv('APPDATA') + r'\anydesk'
rmngBannerPath = os.getenv('APPDATA') + r'\anydesk\msg_thumbnails'
pdPath = os.getenv('ProgramData') + r'\anydesk'
confFile = rmngPath + r'\user.conf'
DETACHED_PROCESS = 8


def check_rem_dir(dir_path):
    if os.path.exists(dir_path):
        print(f'Deleted: {dir_path}')
        try:
            shutil.rmtree(dir_path)
        except Exception as E:
            print(E)
    else:
        print(f'Not exist or already deleted{rmngBannerPath}')


for proc in psutil.process_iter():
    if proc.name() == 'AnyDesk.exe':
        print(f'Found {proc.name()}. Terminating {proc.name()}...')
        proc.kill()
time.sleep(3)

if os.path.exists(confFile):
    try:
        print(f'Open: {confFile}...')
        with open(confFile, 'r', encoding='macgreek') as file:
            data = file.readlines()

        for i, value in enumerate(data):
            keyName = data[i].split('=')[0]
            if keyName == 'ad.invite.created_list_encrypted' or keyName == 'ad.invite.received_list_encrypted':
                print(f"Fix: {data[i]}")
                data[i] = data[i].split('=')[0] + "= \n"

        with open(confFile, 'w', encoding='macgreek') as file:
            file.writelines(data)
        print(f'Fixed: {confFile}')
    except Exception as e:
        print(e)
else:
    print(f'{confFile} not exists')

check_rem_dir(rmngBannerPath)
check_rem_dir(pdPath)
time.sleep(1)

if os.path.exists(anydeskExe):
    print('Running AnyDesk...')
    subprocess.Popen(anydeskExe, creationflags=DETACHED_PROCESS, close_fds=True)
    print('Completed!')
os.system("pause")
