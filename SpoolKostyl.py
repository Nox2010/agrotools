# import os
# import logging
import time
import subprocess


check_interval = 30  # sec

# Для скрытия консоли при выполнении команд
startupinfo = subprocess.STARTUPINFO()
startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
startupinfo.wShowWindow = subprocess.SW_HIDE

# logging.basicConfig(filename=os.getcwd()+'/SpoolKostyl.log', level=logging.INFO, format='%(asctime)s %(message)s',
#                     datefmt='%m/%d/%Y %H:%M:%S')
# logging.info("--------------")
# logging.info("System Startup")
# print("System Startup")


while True:
    subprocess.call(['net', 'start', 'spooler'], startupinfo=startupinfo)
    time.sleep(check_interval)

