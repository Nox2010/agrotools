import logging
import os


filename = os.path.splitext(os.path.basename(__file__))[0]
logging.basicConfig(filename='client.log', level=logging.INFO, format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S')


def parse_line(dictionary, name, line):
    # Parse values after '= ', and append to dictionary
    # values in config file can be empty, so, whe go try.
    try:
        dictionary[name] = line.split('= ')[1].rstrip()
        return dictionary

    except Exception as e:
        logging.error('Cannot parse value from {}'.format(line))
        logging.error(e)
        return None


def get_info_from_file():
    # Read config file, and get values.
    # TODO use %appdata% to shorten path and test on several winOS

    config_path = r'C:\Users\User\AppData\Local\VirtualStore\Program Files (x86)\ШТРИХ-М\Кассир мини\DB\common.txt'
    config_data = {}
    fr_region = False
    egais_region = False

    logging.info('Start read data from {}'.format(config_path))
    if os.path.exists(config_path):
        with open(config_path) as file:
            data = file.readlines()
            for line in data:
                if "<ФР>" in line:
                    fr_region = True
                elif "</ФР>" in line:
                    fr_region = False
                elif "<ЕГАИС>" in line:
                    egais_region = True
                elif "</ЕГАИС>" in line:
                    egais_region = False

                if "Версия " in line:
                    parse_line(config_data, 'soft_version', line)

                if fr_region and "Ком-порт " in line:
                    parse_line(config_data, 'fiscal_com_port', line)

                if fr_region and "Хост " in line:
                    parse_line(config_data, 'fiscal_ip_address', line)

                if egais_region and "НазваниеОрганизации0 " in line:
                    parse_line(config_data, 'egais_ooo', line)

                if egais_region and "ИНН0 " in line:
                    parse_line(config_data, 'egais_inn', line)

                if egais_region and "КПП0 " in line:
                    parse_line(config_data, 'egais_kpp', line)

                if egais_region and "Адрес0 " in line:
                    parse_line(config_data, 'egais_address', line)

        logging.info('File read ok, data parsed ')
        return config_data
    else:
        config_data = {}
        config_data['egais_address'] = filename
        logging.error('File {} not found!'.format(config_path))
        return config_data
