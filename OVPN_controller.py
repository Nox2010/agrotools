import os
import logging
import time
import subprocess

google_dns = "8.8.8.8"
yandex_dns = "77.88.8.8"
ovpn_server = "10.6.74.1"
check_interval = 30  # sec

# Для скрытия консоли при выполнении команд
startupinfo = subprocess.STARTUPINFO()
startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
startupinfo.wShowWindow = subprocess.SW_HIDE

logging.basicConfig(filename=os.getcwd()+'/OVPN_controller.log', level=logging.INFO, format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S')
logging.info("--------------")
logging.info("System Startup")
print("System Startup")


def ping(ip):
    try:
        response = subprocess.call(['ping', ip], stdin=subprocess.PIPE, stdout=subprocess.DEVNULL,
                                   stderr=subprocess.STDOUT, startupinfo=startupinfo)
        print(response)
        return response

    except subprocess.CalledProcessError as e:
        logging.error(e)
        print(e)
        return None


while True:
    # if ovpn server is no responsible - check ping to google/yandex dns. If inet ok - restart ovpn service.
    ovpn_response = ping(ovpn_server)
    if ovpn_response != 0:
        google_response = ping(google_dns)
        yandex_response = ping(yandex_dns)
        if google_response or yandex_response == 0:
            logging.info("OVPN server not reachable, restarting openvpn service.")
            print("OVPN server not reachable, restarting openvpn service.")
            subprocess.call(['net', 'stop', 'openvpnservice'], startupinfo=startupinfo)
            time.sleep(10)
            subprocess.call(['net', 'start', 'openvpnservice'], startupinfo=startupinfo)
            time.sleep(30)
        else:
            logging.info("Google dns and Yandex DNS not reachable.")
            print("Google dns and Yandex DNS not reachable.")
    time.sleep(check_interval)

